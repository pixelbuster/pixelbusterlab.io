---
title: Host your static website in less than 5mins using surge.sh
author: Gururaj Dharani
date: 2019-04-30
hero: ./images/surge-hero.png
excerpt: You might be here filled with inspiration to create and host your static website. Well you are in the right place.
---

If you already have some html and css written for your website and realised hosting is a pain in the a** ? Yes you heard right you can host your static files in less than 2mins.

## Prerequisites

Ensure that you have latest version of NodeJs.

```
node --version
```

## Install Surge

```
npm install --g surge
```

## Deploy

Run below command inside your project root. Enter your email if it prompts to enter your email.

```
cd /project-path
surge
```

If this is your first time you will be asked to enter your email and choose a password.

Your site will be uploaded to server depending on your internet speed and you will be given the site URL at the end of the upload. You can edit the URL if you want before the final step. You will be given a `my-example-cast.surge.sh subdomain`

## Deploy to your domain

You can also deploy your site to your domain using

```
surge /path-to-your-project yourdomain.com
```

Then go to your domain's DNS editor and add a CNAME with hostnames `@` and `www` pointing to `na-west1.surge.sh`

## Setup CNAME file

Specify your domain in the CNAME file created, or use the following command to do the same, in your case use your domain name instead my-example-cast.

```
echo my-example-cast.surge.sh > CNAME
surge
```

In case you want to remove your project

```
surge teardown <project-name>
```

For more `surge --help` or `surge -h`

```
Surge – Single-command web publishing. (v0.20.1)

Usage:
  surge <project> <domain>

Options:
  -a, --add           adds user to list of collaborators (email address)
  -r, --remove        removes user from list of collaborators (email address)
  -V, --version       show the version number
  -h, --help          show this help message

Additional commands:
  surge whoami        show who you are logged in as
  surge logout        expire local token
  surge login         only performs authentication step
  surge list          list all domains you have access to
  surge teardown      tear down a published project
  surge plan          set account plan

Guides:
  Getting started     surge.sh/help/getting-started-with-surge
  Custom domains      surge.sh/help/adding-a-custom-domain
  Additional help     surge.sh/help

When in doubt, run surge from within you project directory.
```

Learn more about [Surge.sh](http://surge.sh/)
