---
title: Angular universal
author: Gururaj Dharani
date: 2019-04-30
hero: images/angular-universal-hero.png
excerpt: The way to convert your existing angular app to server side rendering apps.
---

If you have already written your application in angular and when everything was running smooth you get a requirement to support SEO. Ahh that's possible not to worry.

A normal Angular application executes in the browser, rendering pages in the DOM in response to user actions. Angular Universal executes on the server, generating static application pages that later get bootstrapped on the client. This means that the application generally renders more quickly, giving users a chance to view the application layout before it becomes fully interactive.

Now that we know angular universal requires server, angular has come up with a toolkit called ng-universal which will help us setup our express server. Let's no further waste the time.

## Install ng-universal

```
ng add @nguniversal/express-engine@7.3.9 --clientProject={projectName}
```

project name to be exactly same as title in `app.component.ts`. The command creates the following files in your project.

```
src/
  index.html                 app web page
  main.ts                    bootstrapper for client app
  main.server.ts             * bootstrapper for server app
  style.css                  styles for the app
  app/ ...                   application code
    app.server.module.ts     * server-side application module
server.ts                    * express web server
tsconfig.json                TypeScript client configuration
tsconfig.app.json            TypeScript client configuration
tsconfig.server.json         * TypeScript server configuration
tsconfig.spec.json           TypeScript spec configuration
package.json                 npm configuration
```

## Universal in action

To start rendering your app with universal use the command below:

You can check your package.json might have some scripts newly added when we installed universal, which the below command will use internally.

```
npm run build:ssr
```

Running the above command might be successful if your import import statements are relative,

1. ~~import { AuthService } from "src/app/shared-services/auth.service"~~
2. import { AuthService } from "../../../auth.service"

If you have used absolute import statements you might have to fix all of them before proceeding.

<div className="Image__Large">
  <img
    src="./images/error-statement.png"
    title="Error statements for relative path"
    alt="Error statements for relative path"
  />
</div>

Once all the errors are resolved. Will proceed to the next steps.

## Run your app now

```
npm run serve:ssr
```
Your app should be served on http://localhost:4000/

## Last few steps

Now once your app is served watch for errors on the terminal. You might get errors for *local storage* , *window fn's* might have to wrapped inside isPlatform browser something like this

```
import { isPlatformBrowser } from '@angular/common';

@Injectable({
  providedIn: 'root'
})

export class LocalStoreService {
  constructor(@Inject(PLATFORM_ID) private platformId: Object) { }
  putStore(obj, key) {
    if (isPlatformBrowser(this.platformId)) {
      <!-- your localStorage.methods() here should be wrapped by the condition above -->
      localStorage.setItem(key, 'item');
    }
  }
}

```
Test your app completely and app should be good to be served.
