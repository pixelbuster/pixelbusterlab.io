# Personal blogging by Gatsby

With minimal styling and maximum features — including multiple homepage layouts, built-in social sharing and dark mode — Novela makes it easy to start publishing beautiful articles and stories with Gatsby.

### Build yours

For more information visit the [Theme repository](https://github.com/narative/gatsby-theme-novela)
