module.exports = {
  siteMetadata: {
    title: `Pixelbuster's Blog`,
    name: `Pixelbuster`,
    siteUrl: `http://pixelbuster.in`,
    description: `UI / UX Designer & Javascript developer`,
    hero: {
      heading: `UI / UX Designer & Javascript developer`,
      maxWidth: 652,
    },
    social: [
      {
        name: `twitter`,
        url: `https://twitter.com/gururaj15`,
      },
      {
        name: `github`,
        url: `https://github.com/Gururaj26`,
      },
      {
        name: `instagram`,
        url: `https://instagram.com/pixel_buster`,
      },
      {
        name: `linkedin`,
        url: `https://www.linkedin.com/in/gururaj-dharani-13901055/`,
      },
      {
        name: `dribbble`,
        url: `https://dribbble.com/pixelbuster`,
      },
    ],
  },
  pathPrefix: `/`,
  plugins: [
    {
      resolve: "@narative/gatsby-theme-novela",
      options: {
        contentPosts: "content/posts",
        contentAuthors: "content/authors",
        basePath: "/",
        authorsPage: true,
        sources: {
          local: true,
          // contentful: true,
        },
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Novela by Narative`,
        short_name: `Novela`,
        start_url: `/`,
        background_color: `#fff`,
        theme_color: `#fff`,
        display: `standalone`,
        icon: `src/assets/favicon.png`,
      },
    },
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: "UA-165564801-1",
        head: true,
      },
    },
    {
      resolve: `gatsby-plugin-google-adsense`,
      options: {
        publisherId: `ca-pub-3277432992117548`
      },
    },
    {
      resolve: `gatsby-plugin-netlify-cms`,
      options: {
      },
    },
  ],
};
